package main

import "fmt"

type person struct {
	FirstName string
	LastName  string
	address   contactInfo
}
type contactInfo struct {
	email  string
	mobile int
}

func main() {
	// person1 := person{FirstName: "sangay", LastName: "choden"}
	// fmt.Println(person1)
	// var sangay person
	// fmt.Println(sangay)
	// sangay.FirstName = "sangay"
	// sangay.LastName = "choden"

	// fmt.Println(sangay)
	// fmt.Printf("%+v", sangay)
	sangay := person{
		FirstName: "sangay",
		LastName:  "choden",
		address: contactInfo{
			email:  "san@gmail.com",
			mobile: 12345678,
		},
	}

	// // pointer
	// sangayPointer :=
	// 	&sangay
	// sangay.print()
	// sangayPointer.updateName("choden")
	// sangay.print()
	sangay.updateName("choden")
	sangay.print()

	// fmt.Printf("%+v", sangay)
	// sangay.print()
	// sangay.updateName("sanzin")
	// sangay.print()

	//to print the address
	// fmt.Printf("%p", sangayPointer)
}

func (p person) print() {
	fmt.Printf("%+v", p)
}

// sangay.print()

// func (p person) updateName(newfirstName string) {
// 	// p.fileName = newfirstName
// 	// sangay.print()
// 	// sangay.updateName("sanzin")
// 	// sangay.print()

// }

// pointer is a variable that stores memory address of another variable
// & get the memry address of value the variable is pointing at
// * get value sotred at this memory address
func (pPointer *person) updateName(newfirstName string) {
	(*pPointer).FirstName = newfirstName

}
