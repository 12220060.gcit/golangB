package main

import (
	"testing"
)

func TestNewDeck(t *testing.T) {
	d := newDeck()
	//test1
	if len(d) != 16 {
		t.Errorf("expected deck lenght of 20 buy got %d", len(d))
	}
	//test2
	if d[0] != "Ace of spades" {
		t.Errorf("expected Ace of spades buy got %v", d[0])
	}
	//test3
	if d[len(d)-1] != "four of clubs" {
		t.Errorf("expected Four of clubs got %v", d[len(d)-1])
	}
	// fmt.Println(len(d))

}
