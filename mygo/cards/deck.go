package main

import (
	"fmt"
	"math/rand"
	"os"
	"strings"
)

type deck []string

// reciever type print list of cardss in deck
func (d deck) print() {
	for i, v := range d {
		fmt.Println(i, v)
	}
}

func newDeck() deck {
	dec := deck{}
	suits := []string{"club", "spade", "diamond", "heart"}
	values := []string{"ace", "two", "three", "four"}

	for _, suit := range suits {
		for _, values := range values {
			dec = append(dec, values+" of "+suit)
		}
	}
	return dec
}

// dont want to associate with other existing deck and creating fresh deck

func deal(d deck, handSize int) (deck, deck) {
	cardsInHand := d[:handSize]
	cardsRemaining := d[handSize:]
	return cardsInHand, cardsRemaining
}

//or as a reciever function

// func (d deck) deal(handSize int) (deck, deck) {
// 	cardsInHand := d[:handSize]
// 	cardsRemaining := d[handSize:]
// 	return cardsInHand, cardsRemaining
// }

// helper function to convert deck to string
func (d deck) toString() string {
	return strings.Join([]string(d), ",")

}

//// to convert string to byte
// func (d deck) saveToFile(fileName string) error {
// 	str := d.toString()
// 	//writefile()
// 	err := os.WriteFile(fileName, []byte(str), 0666)
// 	return err
// }

// // readfile
func newDeckFormFile(filename string) deck {
	byteSlice, err := os.ReadFile(filename)
	if err != nil {
		fmt.Println("error:", err)
		os.Exit(1)
	}
	// convert byteslice to deck
	str := strings.Split(string(byteSlice), ",")
	return deck(str)

}

// shuffle
func (d deck) shuffle() {
	for i := range d {
		//generate new random index
		newIndex := rand.Intn(len(d) - 1)
		//swap
		d[i], d[newIndex] = d[newIndex], d[i]
	}
}
