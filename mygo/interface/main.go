package main

import "fmt"

type englishBot struct{}
type dzongkhaBot struct{}

// abstract method where eng and dzo are both interface gi member og bot
// interface is a user defined type that are an abstract set of method
type bot interface {
	greet() string
}

func main() {
	eb := englishBot{}
	db := dzongkhaBot{}

	printGreeting(eb)
	printGreeting(db)
}

func (englishBot) greet() string {
	return "hello"
}

func (dzongkhaBot) greet() string {
	return "kuzuzangpo"
}

// print func has become a common a func for both
func printGreeting(b bot) {
	fmt.Println(b.greet())
}
